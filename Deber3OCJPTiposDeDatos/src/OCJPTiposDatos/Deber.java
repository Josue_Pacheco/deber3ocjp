package OCJPTiposDatos;

public class Deber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		imprimirTiposDeDatos();
		Byte   a = 8;             
		char   b='b';         
		Short  s=16;         
		int    i=32;          
		float  f=32;          
		long   l=64;       
		double d=64;   
		boolean bo=true;
//conversiones tipos de datos primitivos
	d=l;
	//l=f; no vale convertir de long a float 
	f=i;
	i=s;
	
	// s=b; no vale cambiar de byte a short
/* llamada por metodos*/
	float f1;
	double d1;
	f1 = 2.3456f;
	d1=Math.cos(f1); // pasa un flotante a un metdo que espera un double 
	
	/* PROMOCION ARITMETICA */ 
	int aux = 10;
	double aux2 = 20;
	//int r = aux * aux2; no se puede siempre es el tipo mas alto
	double r = aux * aux2;
	short s1 = 3;
	//short s2 = +s1;  se transforma en int
		
	/* CASTING*/ 
	byte b1 = 2;
	char c1 = (char) b1;	
	short s2 = (short) b1;
	s2=(short)c1;
	
	/*OPERADORES UNARIOS*/
	int v = -5;
	System.out.println(v++); //imprime -5 y guarda en -4
	System.out.println(++v); //imprime -3
	System.out.println(v--); //imprime -3 y guarda -4
	System.out.println(--v); //imprime -5
	System.out.println(+v); //multiplica por positivo
	System.out.println(-v); //multiplica por negativo
	System.out.println(~v); //cambia los unos por los ceros en bits.
	
	/*OPERADORES BINARIOS */ 
	int  r2 =  15-19;
	r2= 25+78;
	r2= 78/95;
	r2= 79*74;
	r2= 87%5;
	String s5 = r2 + "resultado"; //+  en datos no primitivos concatena
	
 /*Comparaciones*/
	int  n=3;
	int m =5;
	boolean r4= n>m;
	boolean r5= n<m;
	boolean r6= n>=m;
	boolean r7= n>=m;
	boolean r8= n==m;
	boolean r9= n!=m;
	/*bitwise*/
	int c=2; 
	int b4=3; 
	int d4 = 2&3;
	
	/*operaciones booleanas*/
	boolean t = true;
	boolean f4= false;

	boolean r10; 
	r10= t && f4;
	r10 = t || f4;
	r10= t&f4;
	r10=t^f4;
	r10=f4|t;
	
	/*Tipos de datos no primitivos*/
/*CONVERSION POR REFERENCIA*/
TipoNuevo tn = new TipoNuevo();
TipoViejo tv = tn;


TipoNuevoInterf tni = new TipoNuevoInterf() {
};
TipoViejoInterf tvi= tni;	
	
//TipoViejoInterf tvi2 = tv; solo entre interfaces

TipoViejo tv2 = new TipoViejo();
TipoNuevoInterf tni2 = tv2;
//conversion por metodo 
convertirTipoViejo(tv2);


/*CASTING POR HERENCIA*/
TipoViejo tv3 = new TipoViejo();
TipoNuevo tn3 = (TipoNuevo) tv3;

	}
	
public static TipoNuevo convertirTipoViejo(TipoViejo tv){
	
	TipoNuevo tn = new TipoNuevo();
	tv = tn ;
	return tn;
}	
public static void imprimirTiposDeDatos(){
	
	Byte   a = 8;             
	char   b='b';         
	Short  s=16;         
	int    i=32;          
	float  f=32;          
	long   l=64;       
	double d=64;   
	boolean bo=true;
	
	System.out.println("byte: " + a );
	System.out.println("char: " + b);
	System.out.println("short: "+ s);
	System.out.println("int: " + i);
	System.out.println("float: " + f);
	System.out.println("long: " + l);
	System.out.println("double: " +d);
	System.out.println("boolean: " + bo);
}	
}
